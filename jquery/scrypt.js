/*To run this desktop application type in main folder
nw .
*/

$(function(){

	var operation = "Dodaj";

	var selected_index = -1;

	var Bikes = localStorage.getItem("Bikes");


	Bikes = JSON.parse(Bikes);

	if(Bikes === null)
		Bikes = [];

	function Add(){
		var bike = JSON.stringify({
			ID    : $("#idBike").val(),
			Rodzaj  : $("#typ").val(),
			Rozmiar : $("#size").val(),
			Email : $("#mail").val()
		});
		Bikes.push(bike);
		localStorage.setItem("Bikes", JSON.stringify(Bikes));
		alert("Dodano do formularza.");
		return true;
	}


	function Edit(){
		Bikes[selected_index] = JSON.stringify({
				ID    : $("#idBike").val(),
				Rodzaj  : $("#typ").val(),
				Rozmiar : $("#size").val(),
				Email : $("#mail").val()
			});
		localStorage.setItem("Bikes", JSON.stringify(Bikes));
		alert("Rekord został zaktualizowany.");
		operation = "Dodaj";
		return true;
	}

	function Delete(){
		Bikes.splice(selected_index, 1);
		localStorage.setItem("Bikes", JSON.stringify(Bikes));
		alert("Rekord został usunięty.");
	}

	function List(){
		$("#lista").html("");
		$("#lista").html(
			"<thead>"+
			"	<tr>"+
			"	<th></th>"+
			"	<th>ID</th>"+
			"	<th>Rodzaj</th>"+
			"	<th>Rozmiar</th>"+
			"	<th>Email</th>"+
			"	</tr>"+
			"</thead>"+
			"<tbody>"+
			"</tbody>"
			);
		for(var i in Bikes){
			var rower = JSON.parse(Bikes[i]);
		  	$("#lista tbody").append(
					"<tr>" +
					 "<td>&nbsp;<span title='Edit"+i+"' class='btnEdit glyphicon glyphicon-edit yellow'/>&nbsp;<span title='Delete"+i+"' class='btnDelete glyphicon glyphicon-remove red'/></td>" +
					 "<td>"+rower.ID+"</td>" +
					 "<td>"+rower.Rodzaj+"</td>" +
					 "<td>"+rower.Rozmiar+"</td>" +
					 "<td>"+rower.Email+"</td>" +
		  		"</tr>");
		}
	}

	$("#formBike").submit(function(){
		if(operation == "Dodaj")
			return Add();
		else
			return Edit();
	});

	List();

	$(document).on("click", ".btnEdit", function(){

		operation = "Edytuj";
		selected_index = parseInt($(this).attr("title").replace("Edit", ""));

		var rower = JSON.parse(Bikes[selected_index]);
		$("#idBike").val(rower.ID);
		$("#typ").val(rower.Rodzaj);
		$("#size").val(rower.Rozmiar);
		$("#mail").val(rower.Email);
		$("#idBike").attr("readonly","readonly");
		$("#typ").focus();
	});
//	$(".btnDelete").live("click", function(){...}
	$(document).on("click", ".btnDelete",  function(){
		selected_index = parseInt($(this).attr("title").replace("Delete", ""));
		Delete();
		List();
	});
});
